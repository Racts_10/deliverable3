        
package gofish;

import java.util.ArrayList; 
import java.util.Scanner; 
import java.util.Random;

public class GoFish {
   
    static private ArrayList<Card> card;
    static public Player[] Players;
    static final Random randm = new Random();
 
    
	public static int deckSize()
	{
		return card.size();
        }
        
        public static Card draw()
	{
		return card.remove(randm.nextInt(card.size()));
	}
 
    public static void main(String[] args)
    {
 
        card = new ArrayList<Card>();
        for(int i=0;i<4;i++)
            for(Card c: Card.values())
                card.add(c);
        
        Player computer = new AIPlayer();
        Player human = new HumanPlayer();
        Players = new Player[] {human, computer};
 
        while(Players[0].getNumBooks() + Players[1].getNumBooks() < 13)
        {
            Players[0].haveTurn();
            System.out.println("----------");
            Players[1].haveTurn();
            System.out.println("----------");
        }
 
        int yourScore = Players[0].getNumBooks(); 
        int compScore = Players[1].getNumBooks();
        
        if (yourScore > compScore)
        {  System.out.println("Congratulations, you win "+ yourScore );
        System.out.println("Computer score: " + compScore +"!");}
        
        else if (compScore > yourScore){
            System.out.println("OOPS! You lost to Computer, Hard Luck!! "+ yourScore );
        System.out.println("Computer score: " + compScore +"!");}
        
        else
            System.out.println("Your score matches with the computer,It's a tie "+yourScore);
    }
}
