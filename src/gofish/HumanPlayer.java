package gofish;

import java.util.Scanner;


class HumanPlayer extends Player {
    public void haveTurn()
    {
        Scanner input = new Scanner(System.in);
        boolean playing = true;
        
        do{
            Card books = checkForBooks();
            if(books != null)
                System.out.println("You got a book of " + books);
 
            if (hand.size() == 0)
            {
                System.out.print("Your hand is empty"); //"Go fish!"
                break;
            }
            else
            {
                System.out.print("Your hand:");
                for(Card c: hand)
                    System.out.print(c + " ");
                System.out.println();
            }
 
            System.out.println("Ask opponent for what card?");
 
            Card request;
            try{
                request = Card.valueOf(input.next().toUpperCase());
            }
            catch(IllegalArgumentException e){ //If what you said is not in Card
                System.out.println("Card is not present in this deck. Please try again:");
                continue;
            }
 
            if(!hand.contains(request))
            {
                System.out.println("You cannot ask for a card as you must have one of them. Try again:");
                continue;
            }
 
            System.out.println("You ask for a " + request);
            playing = askFor(request);
        } 
        
        while(playing);
        System.out.println("Go fish!");
        fish();
    }
}
