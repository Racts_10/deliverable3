package gofish;

import java.util.ArrayList;

abstract class Player {
    private int numOfBooks;
    protected ArrayList<Card> hand = new ArrayList<Card>();
    
    public Player()
    {
        for(int i=0;i<8;i++)
            fish();
    }
 
    public boolean hasGiven(Card cardType)
    {
        return hand.contains(cardType);
    }
 
    public ArrayList<Card> giveAll(Card cardType)
    {
        ArrayList<Card> card = new ArrayList<Card>(); 
        for(int i=0;i<hand.size();i++)            
            if (hand.get(i) == cardType)
              card.add(hand.get(i));
        for(int c=0;c<card.size();c++)
            hand.remove(cardType);
        return card;
    }
 
    protected boolean askFor(Card cardType)
    {
        int temp = 0;
        if (this instanceof HumanPlayer)
            temp = 1;
        Player other = GoFish.Players[temp];
 
        if (temp==1)
            ((AIPlayer) other).query.add(cardType);
 
        if (other.hasGiven(cardType))
        {
            for(Card c: other.giveAll(cardType))
                hand.add(c);
            return true;
        }
        else
        {
            return false;
        }
    }
 
    protected void fish()
	    {
	        if (GoFish.deckSize() > 0)
	        	hand.add(GoFish.draw());
	        else
	        	System.out.println("It's impossible because the deck is empty.");
    }
 
    public int getNumBooks()
    {
        return numOfBooks;
    }
 
    protected Card checkForBooks()
    {
        for(Card x: hand)
        {
            int num = 0;
            for(Card y: hand)
              if (x == y)
                  num++;
            if (num == 4)
            {
                for(int i=0;i<4;i++)
                    hand.remove(x);
                numOfBooks++;
                return x;
            }
        }
        return null;
      }
     public abstract void haveTurn();
}
