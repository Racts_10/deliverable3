package gofish;

import java.util.ArrayList;

public class AIPlayer extends Player{
    public ArrayList<Card> query = new ArrayList<Card>();
    private int age = 0;
 
    public void haveTurn()
    {
        boolean play;
        
        do{
            Card book = checkForBooks();
            if(book != null)
                System.out.println("Your opponent got a book of " + book);
            if (hand.size() == 0)
            {
                System.out.print("Opponent's hand is empty.");
                break;
            }
            Card request = aiMagic();
            System.out.println("Opponents asked for:" + request);
            play = askFor(request);
            age++;
        }
        while(play);
        System.out.println("Your opponent goes fishing.");
        fish();
    }

    private Card aiMagic()
    {
        if (age>2)
        {
            query.remove(query.size()-1); 
            age=0;                          
        }
        for(int i=query.size()-1; i>-1; i--)
            if (hand.contains(query.get(i)))
            {
                return query.remove(i); 
            }                           
        return hand.get(GoFish.randm.nextInt(hand.size()));
    }
}
